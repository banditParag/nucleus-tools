for i in $(ls|grep Dockerfile)
do	
	echo ""	
	tool_name=$(echo $i| awk -F"." '{print$2}')
	echo "*********************Building $tool_name*******************************************\n";
	docker build -f $i -t $tool_name:latest .

done;	
